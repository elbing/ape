#### This file is part of the port of the Plan 9 APE libraries to use GCC.
  These libraries are distributed under the terms of the [Plan 9 License](https://bitbucket.org/elbing/ape/src/master/LICENSE),
which you should already have a copy of.

#### APE is the Ansi Posix Environment of Plan 9.
  Plan 9 uses its own system call interface, and APE is provided in order
to provide support for programs written for Posix style systems.

  In the Plan 9 distribution, the APE sources are found under `/sys/src/ape`.
They are compiled and used with Plan 9's native compiler.

  This set of sources is intended to be placed under `/sys/src/gnu/ape`
instead.  It has been modified so as to compile under GCC, and to use
GCC's calling conventions.

  This neccessitated some changes to the
system call wrappers in particular, along with other minor changes
throughout the libraries.

  To build, you will need gcc already installed on your Plan 9 system.
  
  Take it here in binary form:

  [GCC binaries](https://drive.google.com/file/d/0B6YNt0aHfaA0TkREcmdQdWVhY0E/view)

  Uncompress it on top of the tree and you'll have your GCC environment
ready under `/usr/pkg`.

  Once you have GCC, back to APE and then, just
type `mk install` and the libraries will be compiled and installed
under `/$objtype/lib/gnu` (objtype currently must be 386).

  A simple `mk clean` afterwards will remove all intermediate files.
